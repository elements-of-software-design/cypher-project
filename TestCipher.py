#  File: TestCipher.py

#  Description: Encodes and Decodes text utilizing the Rail Fence and Vigenere ciphers

#  Student's Name: Christopher Calizzi

#  Student's UT EID: csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created: 2-4-20

#  Date Last Modified: 2-7-20

#  Input: strng is a string of characters and key is a positive
#         integer 2 or greater and strictly less than the length
#         of strng
#  Output: function returns a single string that is encoded with
#          rail fence algorithm
def rail_fence_encode(strng, key):
    if not strng:
        return "Please Enter a String With Characters"
    if key>1:
        # index of encoded string being assigned
        encoded_index = 0
        # list of encoded letters
        encoded_list = [" "] * len(strng)
        for num in range(key):
            # index of original string being encoded
            original_index = num
            while original_index < len(strng):
                encoded_list[encoded_index] = strng[original_index:original_index + 1]
                original_index += 2 * ((key - 1) - original_index % (key - 1))
                encoded_index += 1
        # final string returned
        encoded = ""
        for char in encoded_list:
            encoded += char
        return encoded
    elif key==1:
        return strng
    else:
        return "Enter key greater than of equal to 1"
#  Input: strng is a string of characters and key is a positive
#         integer 2 or greater and strictly less than the length
#         of strng
#  Output: function returns a single string that is decoded with
#          rail fence algorithm
def rail_fence_decode(strng, key):
    if not strng:
        return "Please Enter a String With Characters"
    if key > 1:
        # index of encoded string being translated
        index = 0
        # list of decoded letters
        decoded_list = [" "] * len(strng)
        for num in range(key):
            # index of decoded string being modified
            decoded_index = num
            while decoded_index < len(strng):
                decoded_list[decoded_index] = strng[index:(index + 1)]
                decoded_index += 2 * ((key - 1) - decoded_index % (key - 1))
                index += 1
        # final string returned
        encoded = ""
        for char in decoded_list:
            encoded += char
        return encoded
    elif key==1:
        return strng
    else:
        return "Enter key greater than of equal to 1"


#  Input: strng is a string of characters
#  Output: function converts all characters to lower case and then
#          removes all digits, punctuation marks, and spaces. It
#          returns a single string with only lower case characters
def filter_string(strng):
    strng = strng.lower()
    output = ""
    for char in strng:
        if 97<=ord(char)<=122:
            output += char
    return output  # placeholder for the actual return statement


#  Input: p is a character in the pass phrase and s is a character
#         in the plain text
#  Output: function returns a single character encoded using the
#          Vigenere algorithm. You may not use a 2-D list
def encode_character(p, s):
    #find ASCII value of encoded character
    char_num = (ord(p)-97)+ord(s)
    if char_num>122:
        char_num -=26
    return chr(char_num)
#  Input: p is a character in the pass phrase and s is a character
#         in the plain text
#  Output: function returns a single character decoded using the
#          Vigenere algorithm. You may not use a 2-D list
def decode_character(p, s):
    # find ASCII value of decoded character
    char_num = ord(s) - (ord(p) - 97)
    if char_num < 97:
        char_num += 26
    return chr(char_num)  # placeholder for actual return statement


#  Input: strng is a string of characters and phrase is a pass phrase
#  Output: function returns a single string that is encoded with
#          Vigenere algorithm
def vigenere_encode(strng, phrase):
    strng = filter_string(strng)
    phrase = filter_string(phrase)
    if not strng or not phrase:
        return "Please Enter Phrase and String With Letters"
    encoded = ""
    for i in range(len(strng)):
        encoded += encode_character(phrase[i%len(phrase):i%len(phrase)+1],strng[i:i+1])
    return encoded


#  Input: strng is a string of characters and phrase is a pass phrase
#  Output: function returns a single string that is decoded with
#          Vigenere algorithm
def vigenere_decode(strng, phrase):
    strng = filter_string(strng)
    phrase = filter_string(phrase)
    if not strng or not phrase:
        return "Please Enter Phrase and String With Letters"
    decoded = ""
    for i in range(len(strng)):
        decoded += decode_character(phrase[i % len(phrase): i % len(phrase) + 1], strng[i: i + 1])
    return decoded


def main():
# prompt the user to enter plain text
    print("Rail Fence Cipher\n")
    strng = input("Enter Plain Text to be Encoded: ")
# prompt the user to enter the key
    key = int(input("Enter Key: "))
# encrypt and print the plain text using rail fence cipher
    print("Encoded Text: " + rail_fence_encode(strng,key) + "\n")
# prompt the user to enter encoded text
    strng = input("Enter Encoded Text to be Decoded: ")
# prompt the user to enter the key
    key = int(input("Enter Key: "))
# decrypt and print the encoded text using rail fence cipher
    print("Decoded Plain Text: " + rail_fence_decode(strng,key) + "\n")
# prompt the user to enter plain text
    print("Vigenere Cipher\n")
    strng = input("Enter Plain Text to be Encoded: ")
# prompt the user to enter pass phrase
    phrase = input("Enter Pass Phrase: ")
# encrypt and print the plain text using Vigenere cipher
    print("Encoded Text: " + vigenere_encode(strng,phrase) + "\n")
# prompt the user to enter encoded text
    strng = input("Enter Encoded Text to be Decoded: ")
# prompt the user to enter pass phrase
    phrase = input("Enter Pass Phrase: ")
# decrypt and print the encoded text using Vigenere cipher
    print("Decoded Plain Text: " + vigenere_decode(strng,phrase) + "\n")
# The line above main is for grading purposes only.
# DO NOT REMOVE THE LINE ABOVE MAIN
if __name__ == "__main__":
    main()